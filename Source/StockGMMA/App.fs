﻿module App

open System
open System.Windows.Forms
open YahooFinance
open Math
open GMMA
open DateTimeExtensions
open Graph

let dailyGmma rawData =
    calculateGMMA rawData

let weeklyGmma rawData =
    let culture = System.Globalization.CultureInfo.InvariantCulture
    let weekData = Seq.groupBy (fun ((d:DateTime), v) -> (d.IsoWeek() :> IConvertible).ToInt32 culture) rawData
                   |> Seq.map (fun (w, s) -> (w, Seq.fold (fun state (d, v) -> v) 0.0 s)) |> Seq.toList
    (weekData, calculateGMMA weekData)

type GmmaApplicationContext() =
    inherit ApplicationContext()
    let dailyData = financeData "^OMX" (new DateTime(2004,06,06)) DateTime.Now
    let dailyGmma = dailyGmma dailyData
    let (weeklyData, weeklyGmma) = weeklyGmma dailyData
    let (dailyForm, _, _) = gmmaVisualiser "Omx30 Daily" "date" "points" dailyData dailyGmma
    let (weeklyForm, _, _) = gmmaVisualiser "Omx30 Weekly" "week" "points" weeklyData weeklyGmma
    let mutable oneClosed = false
    let shouldClose evArgs = match oneClosed with
                             | false -> oneClosed <- true
                             | true  -> Application.ExitThread()
    do dailyForm.Closed.Add(fun evArgs -> shouldClose evArgs)
    do weeklyForm.Closed.Add(fun evArgs -> shouldClose evArgs)
    do printf "Last date:%s\n" (List.rev dailyData |> List.head |> fst |> (fun (d:DateTime) -> d.ToString())) 
