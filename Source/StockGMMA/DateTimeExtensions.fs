﻿module DateTimeExtensions

open System

type IsoWeek =
    {
        year : int;
        week : int;
    }
    member this.int32Representation = this.year * 100 + this.week
    interface IConvertible with
        member this.GetTypeCode() = TypeCode.Object 
        member this.ToBoolean provider = raise (new InvalidCastException())
        member this.ToByte provider = raise (new InvalidCastException())
        member this.ToChar provider = raise (new InvalidCastException())
        member this.ToDateTime provider = raise (new InvalidCastException())
        member this.ToDecimal provider = decimal this.int32Representation
        member this.ToDouble provider = double this.int32Representation
        member this.ToInt16 provider = raise (new InvalidCastException())
        member this.ToInt32 provider = this.int32Representation
        member this.ToInt64 provider = int64 this.int32Representation
        member this.ToSByte provider = raise (new InvalidCastException())
        member this.ToSingle provider = single this.int32Representation
        member this.ToString provider = sprintf "%i.%i" this.year this.week
        member this.ToType (toType, provider) = raise (new InvalidCastException())
        member this.ToUInt16 provider = raise (new InvalidCastException())
        member this.ToUInt32 provider = uint32 this.int32Representation
        member this.ToUInt64 provider = uint64 this.int32Representation

type System.DateTime with
    member this.IsoWeek() =
        let modayIsoWeekOne year =
            let dt = new DateTime(year, 1, 4)
            let delta = match dt.DayOfWeek with
                        | DayOfWeek.Monday    -> -0.
                        | DayOfWeek.Tuesday   -> -1.
                        | DayOfWeek.Wednesday -> -2.
                        | DayOfWeek.Thursday  -> -3.
                        | DayOfWeek.Friday    -> -4.
                        | DayOfWeek.Saturday  -> -5.
                        | _                   -> -6.
            dt.AddDays(delta)
        let (year, week) = match this >= new DateTime(this.Year, 12, 29) with
                           | true  -> let weekOneMonday = modayIsoWeekOne (this.Year + 1)
                                      match this < weekOneMonday with
                                      | true  -> (this.Year, modayIsoWeekOne this.Year)
                                      | false -> (this.Year + 1, weekOneMonday)
                           | false -> let weekOneMonday = modayIsoWeekOne this.Year
                                      match this < weekOneMonday with
                                      | true  -> (this.Year - 1, modayIsoWeekOne (this.Year - 1))
                                      | false -> (this.Year, weekOneMonday)
        { year = year; week = (this - week).Days / 7 + 1 }