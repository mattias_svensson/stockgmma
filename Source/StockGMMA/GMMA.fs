module GMMA

open Math

let calculateGMMA rawData =
    let dates = List.map fst rawData
    let values = List.map snd rawData
    let periods = [3; 5; 8; 10; 12; 15; 30; 35; 40; 45; 50; 60]
    periods |>
    Seq.map (fun period -> (period, ema period values)) |> 
    Seq.map (fun (period, sequence) -> (period, Seq.zip (Seq.skip (period - 1) dates) sequence))
                