﻿module Graph

open System
open System.Drawing
open System.Windows.Forms
open ZedGraph

let chartWindow title =
    let form = new Form(Text = title, Visible = true)
    let graphControl = new ZedGraphControl()
    graphControl.GraphPane.Title.Text <- title
    let setSize (p : ZedGraphControl) (f : Form) = p.Size <- new Size(f.ClientRectangle.Width, f.ClientRectangle.Height)
    form.Controls.Add(graphControl)
    form.Resize.Add(fun evArgs -> setSize graphControl form)
    setSize graphControl form
    (form, graphControl)
    
let visualisePairSequence (graphControl:ZedGraphControl) color sequence =
    let convertToDouble x = match (box x) with
                            | :? DateTime as date -> date.ToOADate()
                            | :? int as a -> double a
                            | :? double as y -> y
                            | _ -> 42.0
    let x = Seq.map fst sequence |> Seq.map convertToDouble |> Seq.toArray
    let y = Seq.map snd sequence |> Seq.toArray
    let list = new PointPairList(x, y)
    let pane = graphControl.GraphPane
    ignore (pane.AddCurve("", list, color, SymbolType.None))
    
let visualisePairSequences graphControl color sequences =
    Seq.iter (visualisePairSequence graphControl color) sequences
    
let gmmaVisualiser title xAxisText yAxisText data gmma  =
    let axisType = match (Seq.head data |> fst |> box) with
                   | :? DateTime -> AxisType.DateAsOrdinal
                   | _ -> AxisType.LinearAsOrdinal
    let xScaleFormat = match (axisType) with
                       | AxisType.DateAsOrdinal -> "yyyy-MM-dd"
                       | _ -> "0000"
    let (form, graphControl) = chartWindow title
    graphControl.GraphPane.XAxis.Type <- axisType
    graphControl.GraphPane.XAxis.Scale.Format <- xScaleFormat
    graphControl.GraphPane.XAxis.Title.Text <- xAxisText
    graphControl.GraphPane.YAxis.Title.Text <- yAxisText
    let earliest = Seq.nth 11 gmma |> snd |> Seq.head |> fst
    let shouldSkip (d, v) = d < earliest
    let truncatedData = Seq.skipWhile shouldSkip data
    let shortGMMA = Seq.take 6 gmma |> Seq.map snd |> Seq.map (Seq.skipWhile shouldSkip)
    let longGMMA = Seq.skip 6 gmma |> Seq.map snd |> Seq.map (Seq.skipWhile shouldSkip)
    visualisePairSequence graphControl System.Drawing.Color.Blue truncatedData
    visualisePairSequences graphControl System.Drawing.Color.Green shortGMMA
    visualisePairSequences graphControl System.Drawing.Color.Red longGMMA
    graphControl.AxisChange()
    graphControl.Invalidate(true)
    (form, graphControl, Seq.map fst truncatedData |> Seq.toList)

type GmmaVisualiserForm(title, xAxisText, yAxisText) as self =
    inherit Form( Text = title, Visible = true )
    let graphControl = new ZedGraphControl()
    do graphControl.GraphPane.Title.Text <- title    
    do graphControl.GraphPane.XAxis.Title.Text <- xAxisText
    do graphControl.GraphPane.YAxis.Title.Text <- yAxisText
    do self.Controls.Add(graphControl)
    do self.Resize.Add(fun evArgs -> self.ResizeChart())
    do self.ResizeChart()

    member private this.ResizeChart() =
        graphControl.Size <- new Size(base.ClientRectangle.Width, base.ClientRectangle.Height)

    member this.VisualiseData (data, gmma) =
        let axisType = match (Seq.head data |> fst |> box) with
                       | :? DateTime -> AxisType.DateAsOrdinal
                       | _ -> AxisType.LinearAsOrdinal
        let xScaleFormat = match (axisType) with
                           | AxisType.DateAsOrdinal -> "yyyy-MM-dd"
                           | _ -> "0000"
        graphControl.GraphPane.XAxis.Type <- axisType
        graphControl.GraphPane.XAxis.Scale.Format <- xScaleFormat
        let earliest = Seq.nth 11 gmma |> snd |> Seq.head |> fst
        let shouldSkip (d, v) = d < earliest
        let truncatedData = Seq.skipWhile shouldSkip data
        let shortGMMA = Seq.take 6 gmma |> Seq.map snd |> Seq.map (Seq.skipWhile shouldSkip)
        let longGMMA = Seq.skip 6 gmma |> Seq.map snd |> Seq.map (Seq.skipWhile shouldSkip)
        visualisePairSequence graphControl System.Drawing.Color.Blue truncatedData
        visualisePairSequences graphControl System.Drawing.Color.Green shortGMMA
        visualisePairSequences graphControl System.Drawing.Color.Red longGMMA
        graphControl.AxisChange()
        graphControl.Invalidate(true)