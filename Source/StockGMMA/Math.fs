﻿module Math

let rec sma period (sequence : seq<float>) =
    let rec slidingWindow period sequence =
        let partition = Seq.truncate period sequence
        seq {    
            match Seq.length partition with
            | x when x < period  -> yield partition
            | _                  -> let remainder = Seq.skip 1 sequence
                                    yield partition
                                    if (not (Seq.isEmpty remainder)) then yield! slidingWindow period remainder
            }
    let periods = slidingWindow period sequence
    seq {for p in periods do 
            let cache = Seq.cache p
            if ((Seq.length cache) = period) then yield (Seq.average cache)}

let ema period sequence =
    let data = 
        let simple = sma period sequence
        let remainder = Seq.skip period sequence
        match Seq.isEmpty simple with
        | true  -> None
        | false -> Some((Seq.head simple), remainder)
    let a = LanguagePrimitives.DivideByInt 2.0 (period + 1)
    let emaCalculation prev current = prev + a * (current - prev)
    Option.map (fun (i, s) -> Seq.scan (emaCalculation) i s) data |>
    Option.fold (fun s d -> d) Seq.empty
