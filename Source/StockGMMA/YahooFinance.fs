﻿module YahooFinance

open System
open System.IO
open System.Globalization

let financeData symbol (fromDate:DateTime) (toDate:DateTime) =
    let fetch (uri : Uri) =
        let req = System.Net.WebRequest.Create(uri)
        use resp = req.GetResponse()
        use stream = resp.GetResponseStream()
        use reader = new StreamReader(stream)
        let html = reader.ReadToEnd()
        html
    let reformat (response:string) =
        let split (mark:char) (data:string) = data.Split(mark) |> Array.toList
        let parseDate data = DateTime.ParseExact(data, "yyyy-MM-dd", null)
        let parseDouble data = Double.Parse(data, CultureInfo.InvariantCulture)
        response |> split '\n' |> List.tail |> List.filter (fun f -> f<>"") |> List.map (split ',')
        |> List.map (fun a -> (parseDate a.[0], parseDouble a.[a.Length - 1]))
        |> List.rev
    let uri =
        let fixMonth (date:DateTime) = date.Month - 1
        new Uri(sprintf "http://ichart.finance.yahoo.com/table.csv?s=%s&b=%i&a=%i&c=%i&e=%i&d=%i&f=%i&g=d&ignore=.csv" symbol fromDate.Day (fixMonth fromDate) fromDate.Year toDate.Day (fixMonth toDate) toDate.Year)
    (fetch >> reformat) uri
