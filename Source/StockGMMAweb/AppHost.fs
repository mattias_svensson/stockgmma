namespace StockGMMA.Web

open System
open ServiceStack.ServiceHost
open ServiceStack.WebHost.Endpoints
open ServiceStack.ServiceInterface

type AppHost() = 
    inherit AppHostBase("Hello F# Services", typeof<HelloService>.Assembly)
    override this.Configure container = ignore()

