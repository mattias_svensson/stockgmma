namespace StockGMMA.Web

open System
open ServiceStack.ServiceHost
open ServiceStack.WebHost.Endpoints
open ServiceStack.ServiceInterface

[<CLIMutable>]
type HelloResponse = { Result:string }
 
[<Route("/hello")>]
[<Route("/hello/{Name}")>]
type Hello() =
    interface IReturn<HelloResponse>
    member val Name = "" with get, set
 
type HelloService() =
    inherit Service()
    member this.Get (request:Hello) =
        {Result = "Hello," + request.Name}