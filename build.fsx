#r @"tools/FAKE/tools/Newtonsoft.Json.dll"
#r @"tools/FAKE/tools/FakeLib.dll"

open Fake

RestorePackages()
let buildDir = @"./build/"

// Targets
Target "Clean" (fun _ -> CleanDir buildDir)

Target "BuildApp" (fun _ ->
    !! @"Source/**/*.fsproj"
    |> MSBuildRelease buildDir "Build"
    |> Log "AppBuild-Output: ")
    
// Dependencies
"Clean" 
    ==> "BuildApp" 

Run "BuildApp"
